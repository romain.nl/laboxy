open Laboxy

let main () =
  Clap.description "Laboxy API Command-Line Interface";

  let base_api_url =
    Clap.mandatory_string
      ~long: "api"
      ~short: 'A'
      ~description: "Base URL of the Laboxy API."
      ()
  in
  let username =
    Clap.mandatory_string
      ~long: "username"
      ~short: 'U'
      ~description: "Username to authenticate as."
      ()
  in
  let password =
    Clap.mandatory_string
      ~long: "password"
      ~short: 'P'
      ~description: "Password for authentication."
      ()
  in

  let command =
    Clap.subcommand [
      (
        Clap.case
          ~description: "Create or update a task."
          "task"
        @@ fun () ->
        let laboxy_id =
          Clap.optional_int
            ~long: "laboxy-id"
            ~description:
              "The Laboxy-chosen identifier for this task. \
               If it exists, the task is updated. If it doesn't, the task is created."
            ()
        in
        let external_id =
          Clap.optional_string
            ~long: "external-id"
            ~short: 'i'
            ~description:
              "An identifier of your choosing for this task. \
               If it exists, the task is updated. If it doesn't, the task is created."
            ()
        in
        let date =
          let default =
            let tm = Unix.(localtime (time ())) in
            let y = tm.tm_year + 1900 in
            let m = tm.tm_mon + 1 in
            let d = tm.tm_mday in
            sf "%04d-%02d-%02d" y m d
          in
          Clap.default_string
            ~long: "date"
            ~short: 'd'
            ~placeholder: "YYYY-MM-DD"
            ~description: "The date at which the task was performed."
            default
        in
        let user_matricule =
          Clap.mandatory_string
            ~long: "user-matricule"
            ~short: 'u'
            ~description: "The matricule of the user that performed the task."
            ()
        in
        let projects_code =
          Clap.list_string
            ~long: "project-code"
            ~short: 'p'
            ~description: "The code of a project to associate to the task."
            ()
        in
        let task_category =
          Clap.mandatory_string
            ~long: "category"
            ~short: 'c'
            ~description: "The category of the task."
            ()
        in
        let name =
          Clap.mandatory_string
            ~long: "name"
            ~short: 'n'
            ~description: "The name of the task."
            ()
        in
        let description =
          Clap.default_string
            ~long: "description"
            ~short: 'D'
            ~description: "A description for this specific task."
            ""
        in
        let value_percent =
          Clap.mandatory_int
            ~description:
              "How much time was spent on this task (percentage of a working day)."
            ~placeholder: "PERCENT"
            ()
        in
        let set_user_service =
          Clap.flag
            ~set_long: "set-user-service"
            ~description:
              "Only used when updating tasks. \
               Update the user service to reflect the new user service
               if it changed since the task was created."
            false
        in
        let set_user_type =
          Clap.flag
            ~set_long: "set-user-type"
            ~description:
              "Only used when updating tasks. \
               Update the user service to reflect the new user service
               if it changed since the task was created."
            false
        in
        `task (
          laboxy_id,
          external_id,
          date,
          user_matricule,
          projects_code,
          task_category,
          name,
          description,
          value_percent,
          set_user_service,
          set_user_type
        )
      );
    ]
  in
  Clap.close ();
  let base_api_url = Uri.of_string base_api_url in
  let config = { base_api_url; username; password } in
  match command with
    | `task (
        laboxy_id,
        external_id,
        date,
        user_matricule,
        projects_code,
        task_category,
        name,
        description,
        value_percent,
        set_user_service,
        set_user_type
      ) ->
        let year, month, day =
          match
            Re.exec_opt
              (Re.compile (Re.Perl.re "^(\\d\\d\\d\\d)-(\\d\\d?)-(\\d\\d?)$"))
              date
          with
            | None ->
                failwith "invalid date (expected YYYY-MM-DD)"
            | Some group ->
                Re.Group.get group 1, Re.Group.get group 2, Re.Group.get group 3
        in
        Laboxy.create_or_update_task
          ?laboxy_id
          ?external_id
          ~year: (int_of_string year)
          ~month: (int_of_string month)
          ~day: (int_of_string day)
          ~user_matricule
          ~projects_code
          ~task_category
          ~name
          ~description
          ~value: (percent value_percent)
          ~set_user_service
          ~set_user_type
          config

let () =
  match Lwt_main.run (main ()) with
    | exception Failure error_message
    | Error error_message ->
        prerr_endline error_message;
        exit 1
    | Ok () ->
        ()
