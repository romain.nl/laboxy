(** Laboxy API implementation. *)

(** {2 Helpers} *)

(** Same as [Lwt.bind]. *)
val (let*): 'a Lwt.t -> ('a -> 'b Lwt.t) -> 'b Lwt.t

(** Same as [Lwt.return]. *)
val return: 'a -> 'a Lwt.t

(** Same as [Printf.sprintf]. *)
val sf: ('a, unit, string, string, string, string) format6 -> 'a

(** {2 Configuration} *)

(** Laboxy API configuration. *)
type config =
  {
    base_api_url: Uri.t; (** Base URL of the Laboxy API. *)
    username: string; (** Username to authenticate as. *)
    password: string; (** Password for authentication. *)
  }

(** {2 Task API} *)

(** Task values, i.e. the time that was spent on a task. *)
type task_value

(** Make a task value from hours and minutes.

    Example: [hours_minutes 1 30] for an hour and a half. *)
val hours_minutes: int -> int -> task_value

(** Make a task value from hours.

    Example: [hours_minutes 1.5] for an hour and a half. *)
val hours: float -> task_value

(** Make a task value from a percentage of the day.

    Example: [percent 25] for a quarter of the day. *)
val percent: int -> task_value

(** Create a new task.

    - [laboxy_id]: the Laboxy-chosen identifier for this task.
      If it exists, the task is updated. If it doesn't, the task is created.

    - [external_id]: an identifier of your choosing for this task.
      If it exists, the task is updated. If it doesn't, the task is created.

    - [year], [month], [day]: the date at which the task was performed.

    - [user_matricule]: the matricule of the user that performed the task.

    - [projects_code] (sic): the list of project codes that this task is associated with.

    - [task_category]: the category of the task.

    - [name]: the name of the task.

    - [description]: a description for this specific task.

    - [value]: how much time was spent on this task.

    - [set_user_service]: only used when updating tasks.
      If [true], the user service is updated to reflect the new user service
      if it changed since the task was created.

    - [set_user_type]: only used when updating tasks.
      If [true], the user type is updated to reflect the new user type
      if it changed since the task was created.

    Returns [Ok ()] if the task was successfully created.
    Returns [Error error_message] otherwise. *)
val create_or_update_task:
  ?laboxy_id: int ->
  ?external_id: string ->
  year: int ->
  month: int ->
  day: int ->
  user_matricule: string ->
  projects_code: string list ->
  task_category: string ->
  name: string ->
  description: string ->
  value: task_value ->
  ?set_user_service: bool ->
  ?set_user_type: bool ->
  config -> (unit, string) result Lwt.t
